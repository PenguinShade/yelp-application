import React, { Component } from 'react';
import './App.css';
import BusinessSearchView from './components/business/BusinessSearchView';
import UserMain from './components/user/UserMain'
import YelpApi from './api/Yelp-Api';
import YelpNavbar from './components/navbar/YelpNavbar';
import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends Component {

  constructor() {
    super();
    this.host = 'http://localhost:3001'
    this.user = { id: 'om5ZiponkpRqUNa3pVPiRg', name: 'Andrea' }
    this.title = 'Simple Yelp Application'
    this.yelpApi = new YelpApi(this.host) //Make an Api instance shared by all components (has same host)
    BusinessSearchView.prototype.yelpApi = this.yelpApi //Pass by props instead? This is probably less flexible
    UserMain.prototype.yelpApi = this.yelpApi;
  }

  render() {
    // console.log(this.state_options)
    return (
      <div>
        {/* <h1 align='center'>{this.title}</h1> */}
        <YelpNavbar />
        <Router>
          <br />
          {/* <Route path='/business-search' component={BusinessSearchView} /> */}
          <Route path='/business-search' render={(props) => (
            <BusinessSearchView user={this.user} />
          )} />
          <Route path='/user' render={(props) => (
            <UserMain user={this.user} />
          )} />
        </Router>
      </div>
    );
  }
}

export default App;
