import axios from 'axios'

//Yelp Api
class YelpApi {
    constructor(host) {
        this.host = host
    }

    getAllBusinessStates(callback) {
        //let url = this.host + '/api/get-business-states';
        let url = '/api/get-business-states';
        axios.get(url)
            .then(res => {
                console.log(res)
                callback(res.data.rows)
            })
    }

    getCitiesInState(state, callback) {
        //Called with a parameters at the end of the endpoint
        //let url = this.host + '/api/get-cities-in-state';
        let url = '/api/get-cities-in-state';
        axios.get(url, {
            params: {
                state: state
            }
        }).then(res => {
                callback(res.data.rows)
            })
    }

    /**
     * TOD;O:
     * @param {string} state 
     * @param {string} city 
     * @param {data => undefined} callback 
     */
    getZipcodes(state, city, callback)
    {
        //let url = this.host + '/api/get-zipcodes';
        let url = '/api/get-zipcodes';
        axios.get(url, {
            params: {
                state: state,
                city: city
            }
        }).then(res => {
                callback(res.data.rows)
            })
        // callback([{zipcode: 123456}, {zipcode: 88833}])
    }

    /**
     * TODO:
     * @param {state: string, city: string, zipcode: string} data 
     * @param {(data) => undefined} callback
     */
    //getAttributes(state, city, zipcode, callback)
    //{
    //    //const url = this.host + '/api/get-categories'
    //    const url = '/api/get-categories'
    //    axios.get(url, {
    //        params: {
    //            state: state,
    //            city: city,
    //            zipcode: zipcode
    //        }
    //    })
    //    .then(res => {
    //        callback(res.data.rows)
    //    })
    //}

    getCategories(state, city, zipcode, callback)
    {
        //const url = this.host + '/api/get-categories'
        const url = '/api/get-categories'
        axios.get(url, {
            params: {
                state: state,
                city: city,
                zipcode: zipcode
            }
        })
        .then(res => {
            callback(res.data.rows)
        })
    }

    /**
     * TODO:
     * @param {num} business_id 
     * @param {(data) => undefined} callback 
     */
    getReviews(business_id, callback)
    {
        //console.log(business_id)
        //const url = this.host + '/api/get-reviews'
        const url = '/api/get-reviews'
        axios.get(url, {
            params: {
                business_id: business_id
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    /**
     * TODO:
     * @param {user_id: num, business_id: num, text: string, rating: num } review 
     */
    postReview(review)
    {
        //let url = this.host + '/api/post-review'
        let url = '/api/post-review'
        axios.post(url, review)
            .then(res => {
                //should just be success
                // console.log(res)
            })
    }

    /**
     * 
     * @param { 
     *          state: string,
     *          city: string,
     *          zipcode: num,
     *          categories: [strings],
     *          attributes: [strings],
     *          price: num
     * } query
     */
    getBusinesses(query, callback)
    {
        //const url = this.host + '/api/get-businesses'
        const url = '/api/get-businesses'
        axios.get(url, {
            params: {
                state: query.state, 
                city: query.city, 
                zipcode: query.zipcode, 
                categories: query.categories, 
                attributes: query.attributes, 
                price: query.price, 
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    getFavoriteBusinesses(userID, callback)
    {
        //const url = this.host + '/api/get-businesses'
        const url = '/api/get-favorite-businesses'
        axios.get(url, {
            params: {
                user_id: userID
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    getUser(user_id, callback)
    {
        //const url = this.host + '/api/get-user'
        const url = '/api/get-user'
        axios.get(url, {
            params: {
                user_id: user_id 
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    getUserIDs(user_name, callback)
    {
        const url = this.host + '/api/get-user-ids'
        axios.get(url, {
            params: {
                user_name: user_name
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    getFriends(user_id, callback)
    {
        const url = this.host + '/api/get-friends'
        axios.get(url, {
            params: {
                user_id: user_id 
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    getFriendReviews(user_id, callback)
    {
        const url = this.host + '/api/get-friend-reviews'
        axios.get(url, {
            params: {
                user_id: user_id 
            }
        }).then(res => {
            callback(res.data.rows)
        })
    }

    postLatLon(user_id, latitude, longitude)
    {
        //let url = this.host + '/api/post-review'
        let url = '/api/post-latlon'
        axios.post(url, {user_id: user_id, latitude: latitude, longitude: longitude})
            .then(res => {
            })
    }
}

export default YelpApi 
