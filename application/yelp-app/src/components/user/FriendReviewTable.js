import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';

class FriendReviewTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cur_userID: null,
            review_options: [],
        }
    }

    getFriendReviews(userID) {
        this.yelpApi.getFriendReviews(userID, (data) => {
            const dateRegex = /\d+-\d+-\d+/
            data.map((value) => {
                //Clean date
                if (value.date_of_review) {
                    var d = dateRegex.exec(value.date_of_review)
                    value.date_of_review = d[0]
                }
                return value
            })
            this.setState({ review_options: data })
        })
    }

    componentDidUpdate(state, props) {
        console.log(this.props.userID)
        if (this.props.userID !== this.state.cur_userID) {
            this.setState({ cur_userID: this.props.userID });
            this.getFriendReviews(this.state.cur_userID);
        }
    }

    render() {
        const columns = [{
            dataField: 'user_name',
            text: 'User Name',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'business_name',
            text: 'Business',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'city',
            text: 'City',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'review_text',
            text: 'Text',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'date_of_review',
            text: 'Date of Review',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }];
        return (
            <div>
                <h3>What are my friends reviewing?</h3>
                <BootstrapTable keyField='user_id' data={this.state.review_options} columns={columns} />
            </div>
        );
    }
}

export default FriendReviewTable;