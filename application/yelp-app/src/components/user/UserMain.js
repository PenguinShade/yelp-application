import React from 'react';
import SetCurrentUser from './SetCurrentUser'
import UserInformation from './UserInformation';
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import FriendsTable from './FriendsTable';
import FavoriteBusinessTable from './FavoriteBusinessTable';
import FriendReviewTable from './FriendReviewTable';

class UserMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selected_userID: null,
        }
        SetCurrentUser.prototype.yelpApi = this.yelpApi;
        UserInformation.prototype.yelpApi = this.yelpApi;
        FriendsTable.prototype.yelpApi = this.yelpApi;
        FavoriteBusinessTable.prototype.yelpApi = this.yelpApi;
        FriendReviewTable.prototype.yelpApi = this.yelpApi;
    }

    onUserSelected(userID) {
        this.setState({ selected_userID: userID });
    }

    render() {
        return (
            <Container fluid={true}>
                <Row>
                    <Col sm={3}>
                        <SetCurrentUser onUserSelected={this.onUserSelected.bind(this)} />
                        <br />
                        <UserInformation userID={this.state.selected_userID} />
                    </Col>
                    <Col sm='auto'>
                        <FriendsTable userID={this.state.selected_userID} />
                        <FavoriteBusinessTable userID={this.state.selected_userID} />
                    </Col>
                    <Col>
                        <FriendReviewTable userID={this.state.selected_userID} />
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default UserMain;