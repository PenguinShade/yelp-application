import React from 'react';
import { Form, Col, Button } from 'react-bootstrap'

class UserInformation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cur_userID: null,
            userData: null,
            disableLatLon: true,
            latitude: null,
            longitude: null,
            // username: '',
            // stars: null,
            // fans: null,
            // yelping_since: '',
            // funny: null,
            // cool: null,
            // useful: null,
        }
        this.longitude = null;
        this.latitude = null;
    }

    getUserInformation(userID) {
        const dateRegex = /\d+-\d+-\d+/
        this.yelpApi.getUser(userID, (data) => {
            data = data[0];
            if (data.yelping_since) {
                var d = dateRegex.exec(data.yelping_since)
                data.yelping_since = d[0]
            }
            this.setState({ userData: data })
        })
    }

    postLatLon(userID, latitude, longitude)
    {
        //Post latitude longitude here
        this.yelpApi.postLatLon(userID, latitude, longitude);
    }

    handleEditClick(e) {
        this.setState({ disableLatLon: !e.target.checked });
    }

    handleUpdate(e) {
        e.preventDefault();
        this.postLatLon(this.latitude, this.longitude);
    }

    onLongitudeChange(e)
    {
        this.longitude = e.target.value;
    }
    onLatitudeChange(e)
    {
        this.latitude = e.target.value;
    }

    // shouldComponentUpdate(state, props)
    // {
    //     console.log('new props: ' + props.userID)
    //     console.log('old props: ' + this.props.userID)
    //     if(props.userID !== this.props.userID)
    //         return true;
    //     return false;
    // }

    componentDidUpdate(state, props) {
        if (this.state.cur_userID !== this.props.userID) {
            this.setState({ cur_userID: this.props.userID })
            this.getUserInformation(this.props.userID);
        }
    }

    zipcode = 'Somezipcode'
    render() {
        return (
            // <div style={{background: '#999'}}>
            <div>
                <h2>User Information</h2>
                <p>userID: {this.props.userID}</p>
                <p>Username: {this.state.userData ? this.state.userData.user_name : ''}</p>
                <p>Stars: {this.state.userData ? this.state.userData.average_stars : ''}</p>
                <p>Fans: {this.state.userData ? this.state.userData.fans : ''}</p>
                <p>Yelping Since: {this.state.userData ? this.state.userData.yelping_since : ''}</p>
                <p>Funny: {this.state.userData ? this.state.userData.funny : ''}</p>
                <p>Cool: {this.state.userData ? this.state.userData.cool : ''}</p>
                <p>Useful: {this.state.userData ? this.state.userData.useful : ''}</p>
                <Form>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridLatitude">
                            <Form.Label>Latitude</Form.Label>
                            <Form.Control type="latitude" onChange={this.onLatitudeChange.bind(this)} disabled={this.state.disableLatLon} placeholder="Enter latitude" />
                        </Form.Group>
                        <Form.Group as={Col} controlId="formBasicChecbox">
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridLongitude">
                            <Form.Label>Longitude</Form.Label>
                            <Form.Control type="longitude" onChange={this.onLongitudeChange.bind(this)} disabled={this.state.disableLatLon} placeholder="Enter longitude" />
                        </Form.Group>
                        <Form.Group as={Col} controlId="updateButton">
                            <Form.Check type="checkbox" label="Edit" onClick={this.handleEditClick.bind(this)} />
                            <Button variant="primary" type="submit" onClick={this.handleUpdate.bind(this)}>
                                Update
                        </Button>
                        </Form.Group>
                    </Form.Row>
                </Form>
                {/* <Form>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="username" placeholder="Enter username" />
                        </Form.Group>

                        <Form.Group as={Col} controlId="formGridStars">
                            <Form.Label>Stars</Form.Label>
                            <Form.Control size='sm' as="select">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>

                    <Form.Group controlId="formGridAddress1">
                        <Form.Label>Address</Form.Label>
                        <Form.Control placeholder="1234 Main St" />
                    </Form.Group>

                    <Form.Group controlId="formGridAddress2">
                        <Form.Label>Address 2</Form.Label>
                        <Form.Control placeholder="Apartment, studio, or floor" />
                    </Form.Group>

                    <Form.Row>
                        <Form.Group as={Col} controlId="formGridCity">
                            <Form.Label>City</Form.Label>
                            <Form.Control />
                        </Form.Group>


                        <Form.Group as={Col} controlId="formGridZip">
                            <Form.Label>Zip</Form.Label>
                            <Form.Control />
                        </Form.Group>
                    </Form.Row>

                    <Form.Group id="formGridCheckbox">
                        <Form.Check type="checkbox" label="Check me out" />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>; */}
            </div>
        );
    }
}

export default UserInformation;