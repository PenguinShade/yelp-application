import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';

class FriendsTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cur_userID: null,
            friend_options: [],
        }
    }

    getFriends(userID) {
        this.yelpApi.getFriends(userID, (data) => {
            const dateRegex = /\d+-\d+-\d+/
            data.map((value) => {
                //Clean date
                if (value.yelping_since) {
                    var d = dateRegex.exec(value.yelping_since)
                    value.yelping_since = d[0]
                }
                return value
            })
            this.setState({ friend_options: data })
        })
    }

    componentDidUpdate(state, props) {
        console.log(this.props.userID)
        if (this.props.userID !== this.state.cur_userID) {
            this.setState({ cur_userID: this.props.userID });
            this.getFriends(this.state.cur_userID);
        }
    }

    render() {
        const columns = [{
            dataField: 'user_name',
            text: 'User Name',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'yelping_since',
            text: 'Yelping Since',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'average_stars',
            text: 'Avg stars',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'review_count',
            text: '# of Reviews',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'fans',
            text: 'Fans',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'funny',
            text: 'Funny',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'cool',
            text: 'Cool',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'useful',
            text: 'Useful',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }];
        return (
            <div>
                <h3>Friends</h3>
                <BootstrapTable keyField='user_id' data={this.state.friend_options} columns={columns} />
            </div>
        );
    }
}

export default FriendsTable;