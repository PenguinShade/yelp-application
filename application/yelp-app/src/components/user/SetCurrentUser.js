import React from 'react';
import { InputGroup, FormControl } from 'react-bootstrap'
import Select from 'react-select'

class SetCurrentUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            cur_username: null,
            userID_options: [],
        }
    }

    getUserIDs(user_name) {
        this.yelpApi.getUserIDs(user_name, (data) => {
            data = data.map((value) => {return value.user_id})
            data = this.createOptionsFromList(data)
            this.setState({userID_options: data})
        })
    }

    createOptionsFromList(array) {
        let items = []
        if (array) {
            let len = array.length
            for (let i = 0; i < len; i++) {
                items.push({ value: i, label: array[i] })
            }
        }
        return items
    }

    onUserSelected(object, action) {
        // console.log(object.label);
        this.props.onUserSelected(object.label);
    }

    onUsernameChange(e) {
        let username = e.target.value;
        if (username) {
            this.setState((state, props) => {
                this.getUserIDs(username);
                return { cur_username: username};
            });
        }
        else
        {
            this.setState({cur_username: '', userID_options: []})
        }
    }

    render() {
        return (
            <div>
                <h1>Set Current User</h1>
                <br />
                <InputGroup size="sm" className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-sm">Username</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm"
                        onChange={this.onUsernameChange.bind(this)} />
                </InputGroup>
                <Select options={this.state.userID_options} onChange={this.onUserSelected.bind(this)}></Select>
            </div>
        );
    }
}

export default SetCurrentUser;