import React from 'react'
// import '../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css';
// import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
// import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable from 'react-bootstrap-table-next';


class FavoriteBusinessTable extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            businesses: [],
            // selected_business: null,
        }
    }

    // round(value, decimals) {
    //     return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    // }

    getFavoriteBusinesses(userID) {
        // console.log(queryData)
        if(userID)
        {
            this.yelpApi.getFavoriteBusinesses(userID, (table) => {
                this.setState({businesses: table})
            })
        }
    }

    componentDidUpdate(newProps, newState) {
        // console.log(this.state)
        if (newProps !== this.props)
            this.getFavoriteBusinesses(this.props.userID)
    }
    // componentDidMount() {
    //     this.getBusinesses(null)
    // }

    render() {
        const columns = [{
            dataField: 'business_name',
            text: 'Business Name',
            headerStyle: (colum, colIndex) => {
                return { width: '80px', textAlign: 'center' };
            }
        }, {
            dataField: 'street_address',
            text: 'Address',
            headerStyle: (colum, colIndex) => {
                return { width: '80px', textAlign: 'center' };
            }
        }, {
            dataField: 'city',
            text: 'City',
            headerStyle: (colum, colIndex) => {
                return { width: '50px', textAlign: 'center' };
            }
        }, {
            dataField: 'state',
            text: 'State',
            headerStyle: (colum, colIndex) => {
                return { width: '50px', textAlign: 'center' };
            }
        }, {
            dataField: 'distance',
            text: 'Distance (miles)',
            headerStyle: (colum, colIndex) => {
                return { width: '30px', textAlign: 'center' };
            }
        }, {
            dataField: 'stars',
            text: 'Stars',
            headerStyle: (colum, colIndex) => {
                return { width: '50px', textAlign: 'center' };
            }
        }, {
            dataField: 'review_count',
            text: '# of Reviews',
            headerStyle: (colum, colIndex) => {
                return { width: '50px', textAlign: 'center' };
            }
        }, {
            dataField: 'reviewrating',
            text: 'Avg Review Rating',
            headerStyle: (colum, colIndex) => {
                return { width: '50px', textAlign: 'center' };
            }
        }, {
            dataField: 'numcheckins',
            text: 'Total Checkins',
            headerStyle: (colum, colIndex) => {
                return { width: '50px', textAlign: 'center' };
            }
        }];
        const rowEvents = {
            onClick: (e, row, rowIndex) => {
                this.onBusinessClick(row)
            }
        }
        if (this.state.businesses)
            return (
                <div>
                    <h3>Favorite Businesses</h3>
                    <BootstrapTable hover bordered condensed scrollTop={ 'Top' }
                        keyField='business_id' data={this.state.businesses} columns={columns} rowEvents={rowEvents} />
                </div>
            );
        return null
    }
}

export default FavoriteBusinessTable; 