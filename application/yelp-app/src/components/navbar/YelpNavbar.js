import React from 'react'
import {Navbar, Nav} from 'react-bootstrap'

class YelpNavbar extends React.Component {

    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="home">Better Yelp</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="home">Home</Nav.Link>
                    <Nav.Link href="business-search">Business</Nav.Link>
                    <Nav.Link href="user">User</Nav.Link>
                </Nav>
                {/* <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-info">Search</Button>
                </Form> */}
            </Navbar>
        )
    }
}

export default YelpNavbar;