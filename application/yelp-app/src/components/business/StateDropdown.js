import React from 'react'
import Select from 'react-select'

class StateDropdown extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            all_business_states: null,
            // selected_state: null,
        }
        this.getAllBusinessStates()
        this.state_options = []
    }

    getAllBusinessStates() {
        this.yelpApi.getAllBusinessStates((states) => {
            // this.setState({ all_business_states: states })
            states = states.map((state_obj) => { return state_obj.state; })
            this.setState((state) => { //Can make simpler without a callback like in getCitiesInState
                this.state_options = this.createOptionsFromList(states)
                return { all_business_states: states }
            })
        })
    }

    onStateSelected(object, action)
    {
        this.props.updateState(object.label)
    }

    createOptionsFromList(array) {
        let items = []
        if (array) {
            let len = array.length
            for (let i = 0; i < len; i++) {
                items.push({ value: i, label: array[i] })
            }
        }
        return items
    }

    render() {
        // console.log(this.state_options)
        return (
            <div>
                <h4>State</h4>
                <Select options={this.state_options} onChange={this.onStateSelected.bind(this)}></Select>
            </div>
        );
    }
}

export default StateDropdown