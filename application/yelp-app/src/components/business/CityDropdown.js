import React from 'react'
import Select from 'react-select'

class CityDropdown extends React.Component
{
    constructor(props) {
        super(props);

        this.state = {
            cities_shown: null,
            shown_for_state: null,
        }
        this.city_options = []
        this.dropdownRef = React.createRef()
    }

    getCitiesInState(selected_state) {
        this.yelpApi.getCitiesInState(selected_state, (cities) => {
            cities = cities.map((city_obj) => { return city_obj.city })
            this.city_options = this.createOptionsFromList(cities)
            this.setState({ cities_shown: cities, shown_for_state: selected_state })
        })
    }

    onCitySelected(e) 
    {
        this.props.updateCity(e.label)
    }

    createOptionsFromList(array) {
        let items = []
        if (array) {
            let len = array.length
            for (let i = 0; i < len; i++) {
                items.push({ value: i, label: array[i] })
            }
        }
        return items
    }

    componentDidUpdate()
    {
        if (this.props.selected_state !== this.state.shown_for_state) //State selected changed
        {
            //Regrab all cities in the new state
            this.getCitiesInState(this.props.selected_state)
            //Clear the selected state from dropdown
            this.dropdownRef.current.setState({value: null})
        }
    }

    render() {
        return (
            <div>
                <h4>City</h4>
                <Select options={this.city_options} onChange={this.onCitySelected.bind(this)} ref={this.dropdownRef}></Select>
            </div>
        );
    }
}

export default CityDropdown 