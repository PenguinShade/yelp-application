import React from 'react'
import StateDropdown from './StateDropdown'
import BusinessTable from './BusinessTable'
import CityDropdown from './CityDropdown';
import ZipcodeDropdown from './ZipcodeDropdown';
import AttributeChecklist from './AttributeChecklist';
import SelectedBusinessView from './SelectedBusinessView'
import CategorySelector from './CategorySelector'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

class BusinessSearchView extends React.Component {
    constructor() {
        super()
        this.state = {
            selectedData: {
                state: null,
                city: null,
                zipcode: null,
                categories: null,
                attributes: null,
                price: null
            },
            selected_business: null,
        }
        // StateCityDropdown.prototype.yelpApi = this.yelpApi
        StateDropdown.prototype.yelpApi = this.yelpApi
        CityDropdown.prototype.yelpApi = this.yelpApi
        ZipcodeDropdown.prototype.yelpApi = this.yelpApi
        CategorySelector.prototype.yelpApi = this.yelpApi
        AttributeChecklist.prototype.yellpApi = this.yelpApi
        SelectedBusinessView.prototype.yelpApi = this.yelpApi
        BusinessTable.prototype.yelpApi = this.yelpApi
    }

    updateState(business_state) {
        this.setState((state) => {
            let newData = state.selectedData
            newData.state = business_state
            //Reset on state change because different cities... etc.
            newData.city = null
            newData.zipcode = null
            newData.categories = null
            return { selectedData: newData }
        })
    }

    updateCity(city) {
        this.setState((state) => {
            let newData = state.selectedData
            newData.city = city
            //Reset on city change because different zips... etc.
            newData.zipcode = null
            newData.categories = null
            return { selectedData: newData }
        })
    }

    updateZipcode(zipcode) {
        this.setState((state) => {
            let newData = state.selectedData
            newData.zipcode = zipcode
            //Reset on city change because different zips... etc.
            newData.categories = null
            return { selectedData: newData }
        })
    }

    updateCategories(categories) {
        this.setState((state) => {
            let newData = state.selectedData
            newData.categories = categories
            return { selectedData: newData }
        })
    }

    updateAttributes(attribute) {
        console.log(attribute)
        this.setState((state) => {
            let newData = state.selectedData
            newData.attributes = attribute
            return { selectedData: newData }
        })
    }

    updateSelectedBusiness(business) {
        this.setState({ selected_business: business })
    }

    render() {
        return (
            <Container fluid={true}>
                <Row>
                    <Col sm={2}>
                        <StateDropdown updateState={this.updateState.bind(this)} />
                        <CityDropdown updateCity={this.updateCity.bind(this)} selected_state={this.state.selectedData.state} />
                        <ZipcodeDropdown updateZipcode={this.updateZipcode.bind(this)} selected_state={this.state.selectedData.state}
                            selected_city={this.state.selectedData.city} />
                        {/* <CategorySelector updateCategories={this.updateCategories.bind(this)} data={this.state.selectedData} /> */}
                        <CategorySelector updateCategories={this.updateCategories.bind(this)} 
                            state={this.state.selectedData.state} city={this.state.selectedData.city}
                            zipcode={this.state.selectedData.zipcode} />
                        <AttributeChecklist updateAttributes={this.updateAttributes.bind(this)}/>
                    </Col>
                    <Col sm={10}>
                        <BusinessTable updateSelectedBusiness={this.updateSelectedBusiness.bind(this)} queryData={this.state.selectedData} />
                        <SelectedBusinessView user={this.props.user} business={this.state.selected_business} />
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default BusinessSearchView