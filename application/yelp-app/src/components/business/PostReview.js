import React from 'react'
// import { InputGroup, FormControl, Button } from 'react-bootstrap'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import Select from 'react-select'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

class PostReview extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            rating: null,
            show_error_message: false,
            if_just_submitted: false,
            cur_business: this.props.business,
        }

        this.formRef = React.createRef()
        this.selectRef = React.createRef()
    }

    onReviewSubmit() {
        if (!this.state.rating) {
            this.setState({ show_error_message: true })
        }
        else { //Succesful submition
            this.postReviewToDatabase()
            this.setState({ show_error_message: false, if_just_submitted: true })
        }
    }

    onRatingChanged(rating_obj) {
        this.setState({ rating: rating_obj.value })
    }

    postReviewToDatabase()
    {
        let review = {
            user_id: this.props.user.id,
            business_id: this.props.business.business_id,
            text: this.formRef.current.value,
            rating: this.state.rating,
        }
        this.yelpApi.postReview(review)
        console.log(review)
    }

    componentDidUpdate() {
        if (this.props.business !== this.state.cur_business) {
            //Business changed
            this.setState({
                rating: null,
                show_error_message: false,
                if_just_submitted: false,
                cur_business: this.props.business,
            })
            //Reset Text field and rating when business changes
            this.formRef.current.value = ''
            this.selectRef.current.setState({value: null})
        }
    }

    render() {
        const options = [
            { value: 1, label: 1 },
            { value: 2, label: 2 },
            { value: 3, label: 3 },
            { value: 4, label: 4 },
            { value: 5, label: 5 },
        ]
        return (

            <Container>
                <Row>
                    <Col>
                        <p>Make Review</p>
                        <InputGroup className="mb-3">
                            <FormControl
                                ref={this.formRef}
                                placeholder="Review Text"
                                aria-label="Review Text"
                                aria-describedby="basic-addon2"
                            />
                            <InputGroup.Append>
                                <Button onClick={this.onReviewSubmit.bind(this)} variant="outline-secondary">Submit</Button>
                            </InputGroup.Append>
                        </InputGroup>
                        <p style={{ color: 'red' }}>{this.state.show_error_message ? 'Must choose a rating before submission' : ''}</p>
                        <p style={{ color: 'green' }}>{this.state.if_just_submitted ? 'Review Submitted!' : ''}</p>
                    </Col>
                    <Col sm={2}>
                        <p>Rating</p>
                        <Select ref={this.selectRef} options={options} placeholder='Rating' onChange={this.onRatingChanged.bind(this)}></Select>
                    </Col>
                </Row>
            </Container>

        );
    }
}

export default PostReview 