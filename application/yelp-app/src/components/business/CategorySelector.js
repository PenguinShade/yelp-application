import React from 'react'
import Select from 'react-select'

class CategorySelector extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            all_categories: [],
        }
        this.dropdownRef = React.createRef()
    }

    getCategories() {
        const data = this.props
        if (!data.zipcode || !data.city || !data.state) //Only need to check first one, but latter is guard code.
        {
            this.setState({ all_categories: [] })
        }
        else {
            // console.log(data)
            //TODO: Make an Api call here
            // const dummyData = [{ value: 0, label: "expensive" }, { value: 1, label: 'roomy' }, { value: 2, label: 'dark' }]
            // this.setState({ all_categories: dummyData })
            this.yelpApi.getCategories(data.state, data.city, data.zipcode, (res) => {
                console.log(res)
                res = res.map(((category, index) => {
                    return {value: index, label: category.category}
                }))
                this.setState({all_categories: res})
            })
        }
    }

    onCategoryChange(object, action) {
        this.props.updateCategories(object)
    }

    componentDidUpdate(prevProps, prevState) {
        //If updating displayed categories. Nothing to do
        if (this.state.all_categories !== prevState.all_categories)
        {
            // console.log('state changed')
            return
        }
        //Maybe later check for other props
        //Note this.props === prevProps doesn't work because react makes seperate prop objects
        if (this.props.zipcode === prevProps.zipcode)
            return 

        this.getCategories()
        this.dropdownRef.current.setState({value: null})
    }

    render() {
        return (
            <div>
                <h4>Categories</h4>
                <Select options={this.state.all_categories} isMulti={true} onChange={this.onCategoryChange.bind(this)}
                    ref={this.dropdownRef} />
            </div>
        );
    }
}

export default CategorySelector 