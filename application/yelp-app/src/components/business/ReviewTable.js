import React from 'react'
import BootstrapTable from 'react-bootstrap-table-next';

class ReviewTable extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            reviews: [],
            shown_for_business: null,
        }
    }

    getReviews(business) {
        //TODO: API call here
        // this.setState({
        //     reviews: [{ review_id: 89, user_name: 'Bill', date: '3/12/19', text: 'This place was really good!' },
        //     { review_id: 12, user_name: 'Carol', date: '2/1/18', text: 'Ice cream tastes like toothpaste' }]
        // });
        const dateRegex = /\d+-\d+-\d+/
        this.yelpApi.getReviews(business.business_id, (table) => {
            table.map((value) => {
                //Clean date
                if (value.date_of_review) {
                    var d = dateRegex.exec(value.date_of_review)
                    value.date_of_review = d[0]
                }
                return value
            })
            this.setState({ reviews: table })
        })

    }

    componentWillMount() {
        this.getReviews(this.props.business)
        this.setState({ shown_for_business: this.props.business })
    }

    componentDidUpdate(prevProps, prevState) {
        if (!this.props.business) {
            if (this.state.reviews.length !== 0) {
                this.setState({ reviews: [] })
            }
        }
        else if (this.props.business !== this.state.shown_for_business) {
            // console.log('didupdate: ' + this.props.business)
            this.getReviews(this.props.business)
            this.setState({ shown_for_business: this.props.business })
        }
    }

    render() {
        // console.log('selected_business change to: ' + this.props.business)
        console.log('render')
        console.log('business: ' + this.props.business.business_id)
        const columns = [{
            dataField: 'user_name',
            text: 'User Name',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'date_of_review',
            text: 'Date',
            headerStyle: (colum, colIndex) => {
                return { width: '100px', textAlign: 'center' };
            }
        }, {
            dataField: 'review_text',
            text: 'Text'
        }];
        return (
            <div>
                <h3>Reviews</h3>
                <BootstrapTable keyField='review_id' data={this.state.reviews} columns={columns} />
            </div>
        );
    }
}

export default ReviewTable 