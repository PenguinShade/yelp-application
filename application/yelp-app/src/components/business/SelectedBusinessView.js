import React from 'react'
import ReviewTable from './ReviewTable'
import PostReview from './PostReview'

class SelectedBusinessView extends React.Component {
    constructor(props) {
        super(props)
        PostReview.prototype.yelpApi = this.yelpApi
        ReviewTable.prototype.yelpApi = this.yelpApi
    }

    render() {
        console.log('selected : ' + this.props.business)
        if (this.props.business)
            return (
                <div>
                    <h3>Selected business: {this.props.business ? this.props.business.business_name : 'N/A'}</h3>
                    <PostReview user={this.props.user} business={this.props.business} />
                    <hr />
                    <ReviewTable business={this.props.business} />
                </div>
            );
        else return null
    }
}

export default SelectedBusinessView


// import StarRatings from 'react-star-ratings'
//Use in the future to display stars 
                // {/* <StarRatings
                //     rating={this.state.rating}
                //     starRatedColor="blue"
                //     changeRating={this.changeRating}
                //     numberOfStars={5}
                //     name='rating'
                // /> */}