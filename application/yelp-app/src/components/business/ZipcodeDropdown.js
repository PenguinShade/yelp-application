import React from 'react'
import Select from 'react-select'

class ZipcodeDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            zipcode_options: []
        }
        this.dropdownRef = React.createRef()
    }

    getZipcodes(state, city) {
        if (!state || !city) { //Missing argument, clear options
            this.setState({ zipcode_options: [] })
        }
        else {
            this.yelpApi.getZipcodes(state, city, (zipcodes) => {
                console.log(zipcodes)
                // this.setState({ all_business_states: states })
                zipcodes = zipcodes.map((zipcode_obj) => { return zipcode_obj.zipcode; })
                zipcodes = this.createOptionsFromList(zipcodes)
                this.setState({ zipcode_options: zipcodes, shown_for_state: state, shown_for_city: city })
            })
        }
    }

    onZipcodeSelected(e) {
        this.props.updateZipcode(e.label)
    }

    createOptionsFromList(array) {
        let items = []
        if (array) {
            let len = array.length
            for (let i = 0; i < len; i++) {
                items.push({ value: i, label: array[i] })
            }
        }
        return items
    }
    //Should component be updated given the nextProps and nextState. 
    //Return true if so, else false will cancel update
    //This is basically not necessary? Will always update but good example.
    shouldComponentUpdate(nextProps, nextState) {
        //Both next state and next city are not null and one changed
        if (nextProps.selected_state && nextProps.selected_city) {
            if (nextProps.selected_state !== this.props.selected_state ||
                nextProps.selected_city !== this.props.selected_city) {
                return true
            }
        }
        //Either next state or next city is null and both were priorly not null
        if ((nextProps.selected_state === null || nextProps.selected_city === null)
            && (this.props.selected_state && this.props.selected_city)) {
            return true
        }
        //Changing zipcode_options
        if (this.state.zipcode_options !== nextState.zipcode_options)
            return true
        return false
    }

    //Either props changed or state changed
    componentDidUpdate(prevProps, prevState) {
        // console.log(prevState)
        // console.log(this.state)
        // console.log(prevProps)
        // console.log(this.props)
        //This is bad code maybe because prevProps really aren't prevProps

        if (prevState.zipcode_options !== this.state.zipcode_options)
            return //Do nothing and let it render
        else 
        {
            //Update zipcodes
            this.getZipcodes(this.props.selected_state, this.props.selected_city)
            //Clear selected zipcodes
            this.dropdownRef.current.setState({ value: null })
        }
    }

    render() {
        // console.log(this.state_options)
        return (
            <div>
                <h4>Zipcode</h4>
                <Select options={this.state.zipcode_options} onChange={this.onZipcodeSelected.bind(this)} 
                    ref={this.dropdownRef}></Select>
            </div>
        );
    }
}

export default ZipcodeDropdown