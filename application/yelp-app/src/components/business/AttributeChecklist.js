import React from 'react'
import Form from 'react-bootstrap/Form'


class AttributeChecklist extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked_attributes: []
        }
    }

    //getAttributes(state, city, zipcode) { //function getattributes()
    //    if (!state || !city || !zipcode) { //Missing argument, clear options in attlist
    //        this.setState({ attlist: [] })
    //    }
    //    else {
    //        //this.yelpApi.getAttributes(state, city, zipcode, (attributes) => {
    //        //    console.log(zipcodes)
    //        //    // this.setState({ all_business_states: states })
    //        //    zipcodes = zipcodes.map((zipcode_obj) => { return zipcode_obj.zipcode; })
    //        //    zipcodes = this.createOptionsFromList(zipcodes)
    //        //    this.setState({ attlist: zipcodes, shown_for_state: state, shown_for_city: city })
    //        //})
    //        console.log("hello")
    //    }
    //}


    //createOptionsFromList(array) {
    //    let items = []
    //    if (array) {
    //        let len = array.length
    //        for (let i = 0; i < len; i++) {
    //            items.push({ value: i, label: array[i] })
    //        }
    //    }
    //    return items
    //}
    //Should component be updated given the nextProps and nextState. 
    //Return true if so, else false will cancel update
    //This is basically not necessary? Will always update but good example.
    //shouldComponentUpdate(nextProps, nextState) {
    //    //Both next state and next city are not null and one changed
    //    if (nextProps.selected_state && nextProps.selected_city) {
    //        if (nextProps.selected_state !== this.props.selected_state ||
    //            nextProps.selected_city !== this.props.selected_city) {
    //            return true
    //        }
    //    }
    //    //Either next state or next city is null and both were priorly not null
    //    if ((nextProps.selected_state === null || nextProps.selected_city === null)
    //        && (this.props.selected_state && this.props.selected_city)) {
    //        return true
    //    }
    //    //Changing attlist
    //    if (this.state.attlist !== nextState.attlist)
    //        return true
    //    return false
    //}

    //Either props changed or state changed
    //componentDidUpdate(prevProps, prevState) {
    //    // console.log(prevState)
    //    // console.log(this.state)
    //    // console.log(prevProps)
    //    // console.log(this.props)
    //    //This is bad code maybe because prevProps really aren't prevProps

    //    if (prevState.attlist !== this.state.attlist)
    //        return //Do nothing and let it render
    //    else 
    //    {
    //        //Update zipcodes
    //        this.getZipcodes(this.props.selected_state, this.props.selected_city)
    //        //Clear selected zipcodes
    //        this.dropdownRef.current.setState({ value: null })
    //    }
    //}

    onAttributeEvent(e) {
        let name = e.target.name;
        let attr_value = 1;
        if(name === "RestaurantsPriceRange2")
        {
            attr_value = parseInt(e.target.value);
        }

        if (e.target.checked == true) {
            this.setState((state) => {
                state.checked_attributes.push({ label: name, attr_value: attr_value });
                this.props.updateAttributes(state.checked_attributes);
                return { checked_attributes: state.checked_attributes };
            })
        }
        else {

            this.setState((state) => {
                let newCheckedAttributes = state.checked_attributes.filter(att => (att.label !== name || att.attr_value !== attr_value));
                this.props.updateAttributes(newCheckedAttributes);
                return {checked_attributes: newCheckedAttributes}
            })
        }
    }


    render() {
        // console.log(this.state_options)
        return (
            <div>
                <h4>Filter by Price</h4>
                <Form> <Form.Group controlId="formBasicChecbox">
                    <Form.Check type="checkbox" label="$" name="RestaurantsPriceRange2" value={1} onClick={this.onAttributeEvent.bind(this)} />
                    <Form.Check type="checkbox" label="$$" name="RestaurantsPriceRange2" value={2} onClick={this.onAttributeEvent.bind(this)} />
                    <Form.Check type="checkbox" label="$$$" name="RestaurantsPriceRange2" value={3} onClick={this.onAttributeEvent.bind(this)} />
                    <Form.Check type="checkbox" label="$$$$" name="RestaurantsPriceRange2" value={4} onClick={this.onAttributeEvent.bind(this)} /></Form.Group>
                </Form>
                <h4>Attributes</h4>
                <Form> <Form.Group controlId="formBasicChecbox">
                    <Form.Check type="checkbox" label="Accepts Credit Cards" name="AcceptsCreditCards" onClick={this.onAttributeEvent.bind(this)} />
                    <Form.Check type="checkbox" label="Wheelchair Accessible" name="WheelchairAccessible" onClick={this.onAttributeEvent.bind(this)} />
                    <Form.Check type="checkbox" label="Hello" name="Hello" onClick={this.onAttributeEvent.bind(this)} /></Form.Group>
                </Form>
            </div>
        );
    }
}

export default AttributeChecklist