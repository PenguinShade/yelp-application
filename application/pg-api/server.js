let express = require('express');
let bodyParser = require('body-parser');
let morgan = require('morgan');
let pg = require('pg');
const PORT = 3001;

let pool = new pg.Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'yelp',
    password: 'postgres',
    port: 5432,
});

pool.connect((err, db, done) => {
    if (err) {
        return console.log(err);
    }
})

let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(morgan('dev'));

function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/** API routing endpoints */
//FIXME: Will have to fix with new database
app.get('/api/get-business-states', (req, res) => { //req = request and res = response
    pool.query('SELECT DISTINCT state FROM business', (err, table) => {
        if (!err)
            res.send(table)
    })
});

//FIXME: Will have to fix with new database
app.get('/api/get-cities-in-state', (req, res) => {
    // console.log(req.query) // this is the json paramter sent in the GET request
    pool.query('SELECT DISTINCT city FROM business WHERE state=\'' + req.query.state + "'", (err, table) => {
        if (!err)
            res.send(table)
        else
            console.log('error happened')
    })
})

//TODO:
app.get('/api/get-zipcodes', (req, res) => {
    // console.log(req.query) // this is the json paramter sent in the GET request
    /* Code will be something like the following */
    // console.log(req.query.city)
    // console.log('HIT ZIPCPODES')
    pool.query('SELECT DISTINCT zipcode FROM business WHERE state=\'' + req.query.state +
        "' AND city='" + req.query.city + "'", (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
})

/**
 * TODO:
 * @param req.query : {state: string, city: string, zipcode: string}
 */
app.get('/api/get-categories', (req, res) => {
    // console.log(req.query)
    // SELECT category FROM business_has_category NATURAL JOIN business
    pool.query('SELECT DISTINCT category FROM business_has_category NATURAL JOIN business WHERE state=\'' + req.query.state +
        "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "'", (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
})

/**
 * TODO:
 * @param { 
 *          state: string,
 *          city: string,
 *          zipcode: num,
 *          categories: [strings],
 *          attributes: [strings],
 *          attributes: {name (string): value (integer)}
 *          price: num
 * } query
 */
app.get('/api/get-businesses', (req, res) => {
    console.log(req.query)
    
    if (req.query.state && req.query.city && req.query.zipcode && req.query.categories && req.query.attributes && req.query.categories.length != 0 && req.query.attributes.length != 0) {

        console.log("categories: ", JSON.parse(req.query.categories[0]).label)
        console.log("attr: ", JSON.parse(req.query.attributes[0]).label)
        

        var str = 'SELECT business_id, business_name, review_count, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business_has_category NATURAL JOIN business WHERE state=\'' + req.query.state +
            "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "' AND category='" + JSON.parse(req.query.categories[0]).label + "'" 

        str += ' INTERSECT SELECT business_id, business_name, review_count, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business NATURAL JOIN business_has_attributes WHERE state=\'' + req.query.state +
        "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode   + "' AND attribute='" + JSON.parse(req.query.attributes[0]).label + "' AND value='" + JSON.parse(req.query.attributes[0]).attr_value + "'"

        for (var i = 1; i < req.query.categories.length;  ++i) 
        {

            str += ' INTERSECT SELECT business_id, business_name, review_count, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business NATURAL JOIN business_has_category WHERE state=\'' + req.query.state +
            "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode   + "' AND category='" + JSON.parse(req.query.categories[i]).label + "'" 
            
        }

        for (var j = 1; i < req.query.attributes.length;  ++j) 
        {

            str += ' INTERSECT SELECT business_id, business_name, review_count, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business NATURAL JOIN business_has_attributes WHERE state=\'' + req.query.state +
            "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode   + "' AND attribute='" + JSON.parse(req.query.attributes[j]).label + "' AND value='" + JSON.parse(req.query.attributes[j]).attr_value + "'"
                
        }

        console.log ("string is : ", str);

        pool.query(str, (err, table) => {
            console.log("I am here in first" , table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
    }

    else if (req.query.state && req.query.city && req.query.zipcode && req.query.categories && req.query.categories.length != 0) {

        var str = 'SELECT business_id, business_name, review_count, business_name, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business_has_category NATURAL JOIN business WHERE state=\'' + req.query.state +
            "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "' AND category='" + JSON.parse(req.query.categories[0]).label + "'";

        for (var i = 1; i < req.query.categories.length; ++i) {
            str += ' INTERSECT SELECT business_id, business_name, review_count, business_name, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business_has_category NATURAL JOIN business WHERE state=\'' + req.query.state +
                "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "' AND category='" + JSON.parse(req.query.categories[i]).label + "'"
        }


        pool.query(str, (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
    }

    else if (req.query.state && req.query.city && req.query.zipcode && req.query.attributes && req.query.attributes.length != 0) {
        console.log("I am in attr")
        var str = 'SELECT business_id, business_name, review_count, business_name, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business_has_attributes NATURAL JOIN business WHERE state=\'' + req.query.state +
            "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "' AND attribute='" + JSON.parse(req.query.attributes[0]).label  + "' AND value='" + JSON.parse(req.query.attributes[0]).attr_value + "'";

        for (var i = 1; i < req.query.attributes.length; ++i) {
            str += ' INTERSECT SELECT business_id, business_name, review_count, business_name, stars, street_address, state, city, zipcode, latitude, longitude, reviewrating FROM business_has_attributes NATURAL JOIN business WHERE state=\'' + req.query.state +
                "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "' AND attribute='" + JSON.parse(req.query.attributes[i]).label + "' AND value='" + JSON.parse(req.query.attributes[j]).attr_value + "'";
        }

        console.log ("string is : ", str);

        pool.query(str, (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
    }
    
    // if condition if there state,cit,zip and atttributes are selected 
    else if (req.query.state && req.query.city && req.query.zipcode) {
        pool.query('SELECT * FROM business WHERE state=\'' + req.query.state + "' AND city='" + req.query.city + "' AND zipcode='" + req.query.zipcode + "'", (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
    }
    else if (req.query.state && req.query.city) {
        pool.query('SELECT * FROM business WHERE state=\'' + req.query.state + "' AND city='" + req.query.city + "'", (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
    }
    else if (req.query.state) {
        pool.query('SELECT * FROM business WHERE state=\'' + req.query.state + "'", (err, table) => {
            // console.log(table)
            if (!err)
                res.send(table)
            else
                console.log('error happened')
        })
    }


})

/**
 * TODO:
 * @param req.query : {business_id: num}
 */
app.get('/api/get-reviews', (req, res) => {
    // Select * from review Natural Join has_reviews LIMIT 10;
    // console.log(req.query)
    pool.query('SELECT * FROM review NATURAL JOIN yelp_user WHERE business_id=\'' + req.query.business_id + "'", (err, table) => {
        // console.log(table)
        if (!err)
            res.send(table)
        else
            console.log('error happened')
    })

})

/**
 * @param req.query : {user_id: num}
 */
app.get('/api/get-user', (req, res) => {
    // Select * from review Natural Join has_reviews LIMIT 10;
    // console.log(req.query)
    pool.query('SELECT * FROM yelp_user WHERE user_id=\'' + req.query.user_id + "'", (err, table) => {
        // console.log(table)
        if (!err)
            res.send(table)
        else
            console.log('error happened')
    })

})

/**
 * @param req.body : {user_id: num, business_id: num, text: string, rating: num}
 */
app.post('/api/post-review', (req, res) => {
    // console.log(req.body)
    var review_id = makeid(22);
    // console.log(review_id)

    pool.query('INSERT INTO review (review_id, stars, useful_count, cool_count, funny_count, review_text, business_id, user_id) VALUES (' + "'" + review_id + "'" + "," + "'" + req.body.rating + "'" + "," + 0 + "," + 0 + "," + 0 + "," + "'" + req.body.text
        + "'" + "," + "'" + req.body.business_id + "'" + ","
        + "'" + req.body.user_id + "'" + ")", (err, table) => {
            console.log(table)
            if (err)
                console.log('error happened')          
        })
})

/**
 * @param req.query : {user_name: string}
 * get-users-ids (input: name, output: all ids with the same name)
 */
app.get('/api/get-user-ids', (req, res) => {
    // Select user_id from yelp_user where user_name='Andrea';
    // console.log(req.query)
    pool.query('SELECT user_id FROM yelp_user WHERE user_name=\'' + req.query.user_name + "'", (err, table) => {
        // console.log(table)
        if (!err)
            res.send(table)
        else
            console.log('error happened')
    })

})

/**
 * @param req.query : {user_name: string}
 *  get-friends (input: userID, output: all friends of that user)
 */
app.get('/api/get-friends', (req, res) => {
    // console.log(req.query)
    pool.query('SELECT * FROM yelp_user WHERE user_id IN  (Select friends_user_id FROM yelp_user NATURAL JOIN friend WHERE user_id=\'' + req.query.user_id + "'" + ")" , (err, table) => {
        // console.log(table)
        if (!err)
            res.send(table)
        else
            console.log('error happened')
    })

})


/**
 * @param req.query : {user_name: string}
* API GET: get-friend-reviews (input: business_id, user_id, output: friendName, review data)
 */
app.get('/api/get-friends_reviews', (req, res) => {
    console.log(req.query)
    var str = 'SELECT review_text, user_name FROM review NATURAL JOIN yelp_user WHERE user_id IN  (Select friends_user_id FROM friend WHERE user_id=\'' + req.query.user_id + "')"+ ' AND business_id=\'' + req.query.business_id + "'"
    console.log(str)
    pool.query( str, (err, table) => {
        // console.log(table)

        if (!err)
            res.send(table)
        else
            console.log('error happened')
    })

})
/**
 * TODO:
 * @param req.body : {user_id: string, longitude: num, latitude: num}
 */

app.post('/api/update-user', (req, res) => {
    console.log(req.body)
    var str = 'UPDATE yelp_user SET longitude=\'' + req.body.longitude + "'" + ",'" + 'latitude=\''+ req.body.latitude + "' WHERE user_id='" + req.body.user_id + "'"
    console.log(str)

    pool.query(str, (err, table) => {
            console.log(table)
            if (err)
                console.log('error happened')          
        })
})
// -- INSERT INTO favorite_business (user_id, business_id)
// -- VALUES ('fgfdasdf','dfasdwe'); 

// -- DELETE FROM favorite_business WHERE user_id='fgfdasdf' AND business_id='dfasdwe'; 

/**
 * @param req.body : {user_id: string, business_id: string}
 */

app.post('/api/insert-favorite-business', (req, res) => {
    console.log(req.body)
    var str = 'INSERT INTO favorite_business (user_id, business_id) VALUES (' + "'" + req.body.user_id + "'" + "," + "'" + req.body.business_id + "'" + ")"
    console.log(str)

    pool.query(str, (err, table) => {
            console.log(table)
            if (err)
                console.log('error happened')          
        })
})

/**
 * @param req.body : {user_id: string, business_id: string}
 */

app.post('/api/delete-favorite-business', (req, res) => {
    console.log(req.body)
    var str = 'DELETE FROM favorite_business WHERE user_id=\'' + req.body.user_id + "'" + ",'" + 'latitude=\''+ req.body.latitude + "' WHERE user_id='" + req.body.user_id + "'"
    console.log(str)

    pool.query(str, (err, table) => {
            console.log(table)
            if (err)
                console.log('error happened')          
        })
})
app.listen(PORT, () => console.log('Listening on port ' + PORT));