ALTER TABLE review
ADD COLUMN user_id VARCHAR(50);
ALTER TABLE review
ADD COLUMN business_id VARCHAR(50);

UPDATE review
SET business_id = has_reviews.business_id,
 user_id = has_reviews.user_id
FROM has_reviews
WHERE has_reviews.review_id = review.review_id;

ALTER TABLE review
ADD CONSTRAINT user_review_fkey
FOREIGN KEY (user_id) REFERENCES yelp_user;

ALTER TABLE review
ADD CONSTRAINT business_review_fkey
FOREIGN KEY (business_id) REFERENCES business;

-- Select * from review limit 10;
 
DROP TABLE has_reviews;
