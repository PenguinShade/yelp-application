-- \c yelp;

CREATE TABLE Yelp_user(
    user_id         VARCHAR(50),
    user_name       VARCHAR(70),
    useful          INTEGER,
    average_stars   FLOAT,
    funny           INTEGER,
    yelping_since   DATE,
    cool            INTEGER,
    fans            INTEGER,
    review_count    INTEGER,
    PRIMARY KEY (user_id)
);

-- CREATE TABLE Friend (
--     user_id_1       VARCHAR(50),
--     user_id_2       VARCHAR(50),
--     PRIMARY KEY(user_id_1),
    -- FOREIGN KEY(user_id_1) REFERENCES Yelp_user(user_id),
    -- FOREIGN KEY(user_id_2) REFERENCES Yelp_user(user_id),
--     CHECK(user_id_1 < user_id_2) --How to check compare two strings
-- );

-- Friendships don't have to be two-way. A can friend B and B not friend A.
CREATE TABLE Friend(
    user_id         VARCHAR(50),
    friends_user_id VARCHAR(50),
    PRIMARY KEY(user_id, friends_user_id),
    FOREIGN KEY(user_id) REFERENCES Yelp_user(user_id),
    FOREIGN KEY(friends_user_id) REFERENCES Yelp_user(user_id)
);

CREATE TABLE Business(
    business_id     VARCHAR(50),
    review_count    INTEGER,
    neighborhood    VARCHAR(100),
    business_name   VARCHAR(200),
    stars           FLOAT,
    street_address  VARCHAR(200),
    state           CHAR(2),
    city            VARCHAR(100),
    zipcode         CHAR(5),
    latitude        FLOAT,
    longitude       FLOAT,
    PRIMARY KEY(business_id)
);

CREATE TABLE Review(
    review_id       VARCHAR(50),
    business_id     VARCHAR(50),
    user_id         VARCHAR(50),
    stars           FLOAT,
    date_of_review  DATE DEFAULT NOW() -- DEFAULT CURRENT_DATE, -- this is not tested
    useful_count    INTEGER,
    cool_count      INTEGER,
    funny_count     INTEGER,
    review_text     VARCHAR(5000),
    PRIMARY KEY(review_id)
    FOREIGN KEY(user_id) REFERENCES Yelp_user(user_id),
    FOREIGN KEY(business_id) REFERENCES Business(business_id),
);

-- Depreciated
-- CREATE TABLE has_reviews(
--     review_id       VARCHAR(50),
--     business_id     VARCHAR(50),
--     user_id         VARCHAR(50),
--     PRIMARY KEY(review_id)
--     FOREIGN KEY(user_id) REFERENCES Yelp_user(user_id),
--     FOREIGN KEY(business_id) REFERENCES Business(business_id),
--     FOREIGN KEY(review_id) REFERENCES Review(review_id)
-- );

CREATE TABLE Hours(
    business_id     VARCHAR(50),
    day             CHAR(9),
    hours_range     CHAR(11),
    PRIMARY KEY(business_id, day),
    FOREIGN KEY(business_id) REFERENCES Business(business_id)
);

CREATE TABLE Check_in(
    business_id     VARCHAR(50),
    num_of_checkins INTEGER,
    day             CHAR(9),
    time            CHAR(5),
    PRIMARY KEY(business_id, day, time),
    FOREIGN KEY(business_id) REFERENCES Business(business_id)
);

CREATE TABLE Business_has_attributes(
    business_id     VARCHAR(50),
    attribute       VARCHAR(50),
    value           INTEGER, -- 0 is false, 1 true, use as number else
    value2          VARCHAR(75),
    PRIMARY KEY(business_id, attribute)
);

CREATE TABLE Business_has_category(
    business_id     VARCHAR(50),
    category        VARCHAR(50),
    PRIMARY KEY(business_id, category),
    FOREIGN KEY(business_id) REFERENCES Business(business_id)
);
