ALTER TABLE business
ADD reviewrating FLOAT;

UPDATE business
SET reviewrating = 
(SELECT AVG(review.stars)
FROM review
WHERE reviews.business_id = business.business_id)
