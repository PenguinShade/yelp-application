CREATE OR REPLACE FUNCTION updateReviewRating() RETURNS TRIGGER AS '
BEGIN
	UPDATE business
	SET reviewrating = 
	(SELECT AVG(review.stars)
	FROM review
	WHERE review.business_id = business.business_id)
	WHERE NEW.business_id = business.business_id;
	RETURN NEW;
END
' LANGUAGE plpgsql;

CREATE TRIGGER updateReviewRatingOnInsert
AFTER INSERT ON review
FOR EACH ROW
WHEN(NEW.review_id IS NOT NULL)
EXECUTE PROCEDURE updateReviewRating();

--Test statement
INSERT INTO review(review_id,stars) VALUES('Now, this is a story all about how My life got fl', 5);
INSERT INTO has_reviews(review_id,business_id,user_id) VALUES('Now, this is a story all about how My life got fl','P_PCpidHJQksW2-ufqoz4w','om5ZiponkpRqUNa3pVPiRg');