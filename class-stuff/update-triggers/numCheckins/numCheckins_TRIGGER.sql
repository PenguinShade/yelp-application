CREATE OR REPLACE FUNCTION updateNumCheckins() RETURNS TRIGGER AS '
BEGIN
	UPDATE business as B1
	SET numcheckins=(SELECT numcheckins FROM business as B2 WHERE B1.business_id=B2.business_id)+NEW.num_of_checkins-OLD.num_of_checkins
	WHERE NEW.business_id=B1.business_id;
	RETURN NEW;
END
' LANGUAGE plpgsql;

CREATE TRIGGER newCheckin
AFTER UPDATE ON check_in
FOR EACH ROW

-- Test statements
SELECT * FROM business WHERE business_id='PWs6xJQJPHxknd3FcdKn9Q';
SELECT * FROM check_in WHERE business_id='PWs6xJQJPHxknd3FcdKn9Q';

UPDATE check_in
SET num_of_checkins=4
WHERE business_id='PWs6xJQJPHxknd3FcdKn9Q' AND check_in.day='Wednesday';

-- Should see business increase to 10 for numCheckins
