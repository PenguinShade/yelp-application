ALTER TABLE business
ADD COLUMN numCheckins INTEGER;

UPDATE business as B
SET numCheckins = (SELECT SUM(num_of_checkins)
		   FROM check_in as C
		   WHERE C.business_id = B.business_id)
