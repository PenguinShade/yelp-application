CREATE OR REPLACE FUNCTION updateReviewCount() RETURNS TRIGGER AS '
BEGIN
	UPDATE business
	SET review_count = 
	(SELECT COUNT(*)
	FROM review
	WHERE review.business_id = business.business_id)
	WHERE NEW.business_id = business.business_id;
	RETURN NEW;
END
' LANGUAGE plpgsql;

CREATE TRIGGER updateReviewCountOnInsert --I suppose it could be possible combine this trigger with newReview
AFTER INSERT ON review
FOR EACH ROW
WHEN(NEW.review_id IS NOT NULL)
EXECUTE PROCEDURE updateReviewCount();

-- INSERT INTO review(review_id,stars) VALUES('now, this is a story all about how My life got fl', 5);
-- INSERT INTO has_reviews(review_id,business_id,user_id) VALUES('now, this is a story all about how My life got fl','P_PCpidHJQksW2-ufqoz4w','om5ZiponkpRqUNa3pVPiRg');