-- Should already exist in table
-- ALTER TABLE business  
-- ADD review_count INTEGER

UPDATE business
SET review_count = 
(SELECT COUNT(*)
FROM review
WHERE review.business_id = business.business_id)