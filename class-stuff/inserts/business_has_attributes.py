import json
import psycopg2
import config

def connection1(conn, cur, sql_str):
    try:
        cur.execute(sql_str)
    except Exception as e:
        print(e)
        print("Insert to business failed!")
    conn.commit()


def iterdict(d, conn, cur, busID):
    for k, v in d.items():
        if isinstance(v, dict):
            iterdict(v, conn, cur, busID)
        elif isinstance(v,str):
         # enter in value2 and enter -ve val in value1
            v= v.replace('-', ',')
            v = cleanStr4SQL(v)
            sql_str = "INSERT INTO business_has_attributes (business_id, attribute, value2) VALUES ('" + cleanStr4SQL(busID) + "','" + cleanStr4SQL(k)+ "','" + cleanStr4SQL(v) + "');"
            connection1(conn, cur, sql_str)
        elif isinstance(v,bool) and v == True:
         #else if it is a boolean
        #enter 1 for true values and ignore false values
            sql_str = "INSERT INTO business_has_attributes (business_id, attribute, value) VALUES ('" + cleanStr4SQL(busID) + "','" + cleanStr4SQL(k)+ "','" + str(1) + "');"
            connection1(conn, cur, sql_str)
        elif not isinstance(v,bool):
    #else if e have integer
           #insert in the value1 and keep value2 empty
            sql_str = "INSERT INTO business_has_attributes (business_id, attribute, value) VALUES ('" + cleanStr4SQL(busID) + "','" + cleanStr4SQL(k)+ "','" + str(v) + "');"
            connection1(conn, cur, sql_str)
                  

def cleanStr4SQL(s):
    return s.replace("'","`").replace("\n"," ")

def int2BoolStr (value):
    if value == 0:
        return 'False'
    else:
        return 'True'

def insert2BusinessTable():
    #reading the JSON file
    with open('../../../data/raw/yelp_business.JSON','r') as f:    #TODO: update path for the input file
        #outfile =  open('./yelp_business.SQL', 'w')  #uncomment this line if you are writing the INSERT statements to an output file.
        line = f.readline()
        count_line = 0

        #connect to yelpdb database on postgres server using psycopg2
        #TODO: update the database name, username, and password
        try:
            # conn = psycopg2.connect("dbname='" + dbname + "' port='5433' user='postgres' host='localhost' password='"+ password +"'")
            conn = psycopg2.connect("dbname='yelp' port='5432' user='postgres' host='localhost' password='postgres'")
        except:
            print('Unable to connect to the database!')
        cur = conn.cursor()

        while line:
            data = json.loads(line)
            # Generate the INSERT statement for the cussent business
            # TODO: The below INSERT statement is based on a simple (and incomplete) businesstable schema. Update the statement based on your own table schema and
            # include values for all businessTable attributes
            attr_dict = data['attributes']
            iterdict(attr_dict, conn, cur, data["business_id"])
            #for eachCat in data["attributes"]:
            #    
            #    sql_str = "INSERT INTO business_has_attributes (business_id, attribute, value) VALUES ('" + cleanStr4SQL(data["business_id"]) + "','" + cleanStr4SQL(eachCat) + "');"
            #    try:
            #        cur.execute(sql_str)
            #    except Exception as e:
            #        print(e)
            #        print("Insert to business failed!" + cleanStr4SQL(data["name"]))
            #    conn.commit()
            # optionally you might write the INSERT statement to a file.
            # outfile.write(sql_str)

            line = f.readline()
            count_line +=1

        cur.close()
        conn.close()

    print(count_line)
    #outfile.close()  #uncomment this line if you are writing the INSERT statements to an output file.
    f.close()


insert2BusinessTable()
