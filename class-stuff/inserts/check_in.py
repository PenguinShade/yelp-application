import json
import psycopg2

def iterdict(d, conn, cur, busID):
    for day, hour in d.items():
        sql_str = "INSERT INTO hours (business_id, day, hours_range) VALUES ('" + cleanStr4SQL(busID) + "','" + cleanStr4SQL(day) + "','" + cleanStr4SQL(time) + "');"
        try:
            cur.execute(sql_str)
        except Exception as e:
            print(e)
            print("Insert to business failed!")
        conn.commit()

def cleanStr4SQL(s):
    return s.replace("'","`").replace("\n"," ")

def int2BoolStr (value):
    if value == 0:
        return 'False'
    else:
        return 'True'

def insert2BusinessTable():
    #reading the JSON file
    with open('../../../data/raw/yelp_checkin.JSON','r') as f:    #TODO: update path for the input file
        #outfile =  open('./yelp_business.SQL', 'w')  #uncomment this line if you are writing the INSERT statements to an output file.
        line = f.readline()
        count_line = 0

        #connect to yelpdb database on postgres server using psycopg2
        #TODO: update the database name, username, and password
        try:
            conn = psycopg2.connect("dbname='yelp' port='5432' user='postgres' host='localhost' password='postgres'")
        except:
            print('Unable to connect to the database!')
        cur = conn.cursor()

        while line:
            #user_id,  name,  yelping_since,  review_count,  fans,  average_star,  funny,  useful,  cool,  friends	
            data = json.loads(line)
            # Generate the INSERT statement for the cussent business
            # TODO: The below INSERT statement is based on a simple (and incomplete) businesstable schema. Update the statement based on your own table schema and
            # include values for all businessTable attributes
            time_dict = data["time"]
            for day, values in time_dict.items():
                for hour, checks in values.items():
                    sql_str = "INSERT INTO check_in (business_id, num_of_checkins, day, time) VALUES ('" + cleanStr4SQL(data["business_id"]) + "','" + str(checks) + "','" + cleanStr4SQL(day) + "','" + cleanStr4SQL(hour) + "');"
                    try:
                        cur.execute(sql_str)
                    except Exception as e:
                        print(e)
                        print("Insert to checkin failed!" + cleanStr4SQL(data["business_id"]))
                    conn.commit()
            #sql_str = "INSERT INTO hours (business_id, day, hours_range) " \
            #            "VALUES ('" + cleanStr4SQL(data["business_id"]) + "','" + cleanStr4SQL(data["name"]) + "','" + str(data["useful"]) + "','" + str(data["average_stars"]) + "','" + \
            #            str(data["funny"]) + "','" + str(data["yelping_since"]) + "','" + str(data["cool"]) + "'," + str(data["fans"]) + "," + \
            #            str(data["review_count"]) + ");"
            #try:
            #    cur.execute(sql_str)
            #except Exception as e:
            #    print(e)
            #    print("Insert to user failed!" + cleanStr4SQL(data["name"]))
            #conn.commit()
            # optionally you might write the INSERT statement to a file.
            # outfile.write(sql_str)

            line = f.readline()
            count_line +=1

        cur.close()
        conn.close()

    print(count_line)
    #outfile.close()  #uncomment this line if you are writing the INSERT statements to an output file.
    f.close()


insert2BusinessTable()
