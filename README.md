# 451-yelp-application

An application which uses yelp data and functions similarly.

## Install

### Postgresql database
1. Download and install here https://postgresql.org
1. Locate your postgres config file with `locate pg_hba.conf`
2. Change line with  
    ``` 
    local   all             postgres                                peer 
    ``` 
    to
    ```
    local   all             postgres                                trust 
    ```
1. Use psql with `psql -U postgres`
4. Instead of steps 2-4 you can just login to server as postgres by: `su - postgres; psql`
5. Make a database: `CREATE DATABASE yelp;`
1. Now here you can load in the dump file into the yelp database by navigating to the file, then `psql database-name < dump.sql`
6. Connect to database: `\c yelp`
7. Load in tables: `\i table.SQL`

### Web Application
The application is broken up into two parts. The backend (called pg-api) and the front end (yelp-app)
1. Start up the pg-api
   1. Cd into pg-api directory
   1. `npm install`
   1. `npm start`
1. Start front-end React app
    1. In different terminal Cd into yelp-app
    1. `npm install`
    1. `npm start`

## Useful Commands
1. Look at tables made: `\d`
2. Look at data in tables: `\d table-name`

## Insert Statements
1. Create SQL tables in postgre with correct order
2. Install psycopg2 `pip install psycopg2`
3. Adjust the sql_str in the python code to match the database
4. Run each python code

## Useful Links

https://www.tutorialspoint.com/postgresql/postgresql_schema.htm - Lists all SQL commands with explanation and examples.

https://react-select.com/props - Documentation of React-Select which is the dropdown used

https://react-bootstrap.github.io/getting-started/introduction/ - Documentation for React Bootstrap which will be used

https://expressjs.com/en/guide/routing.html - Documenation for Express routing used for backend server




